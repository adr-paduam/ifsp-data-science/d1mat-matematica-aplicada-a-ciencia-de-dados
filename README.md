# D1MAT - MATEMÁTICA APLICADA À CIÊNCIA DE DADOS

Mathematics fundamentals for Data Science

Primary language: [Python](https://www.python.org)

Main library: [SymPy](https://docs.sympy.org/latest/index.html)